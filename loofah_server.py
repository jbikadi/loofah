from flask import Flask, request, send_from_directory
from loofah.soap import SOAPEnvelope


def _D(n): return [x for x in dir(n) if not x.startswith('_')]


app = Flask(__name__, static_folder='')


@app.route('/')
def landing_page():
    return send_static_file('index.html')


@app.route('/static/<path:path>')
def send_static_file(path):
    return send_from_directory('static', path)


# @app.route('/', defaults={'path': ''})
@app.route('/<path:path>', methods=['GET', 'POST'])
def hello(path):
    print('-------------------------')
    headers = request.headers
    endpoint = f"//{headers['Host']}/{path}"
    # print(request.headers)
    # print(_D(request))

    soap = SOAPEnvelope(endpoint, request.stream.read())

    print('-------------------------')
    print(f'Request for "{path}"')
    print('-------------------------')
    return "Hello World!"


if __name__ == '__main__':
    app.run(debug=True)
