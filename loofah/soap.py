from .exceptions import ICantEvenException

from lxml import etree


def _D(n): return [x for x in dir(n) if not x.startswith('_')]


# I need a way to add multispeak namespaces later
NAMESPACES = {
    # None: '',
    'soap': 'http://schemas.xmlsoap.org/soap/envelope/',
    # 'msthree': 'http://www.multispeak.org/Version_3.0',
}


def getXpathResult(element, xpath, namespaces=None):
    results = element.xpath(xpath, namespaces=namespaces)
    # if len(results) > 1:
    #     raise ICantEvenException('Unexpected quantity of SOAP elements')
    # else:
    # return results[0]
    if results:
        output = list()
        for element in results:
            for child in element:
                output.append(child)
        return output
    else:
        return None


def getChildren(element):
    # if element and element.has_children
    return element.children


class SOAPEnvelope ():
    def __init__(self, endpoint, content):
        self.endpoint = endpoint
        self.tree = etree.fromstring(content)

        self.header = getXpathResult(
            self.tree, '//soap:Header', namespaces=NAMESPACES)
        self.body = getXpathResult(
            self.tree, '//soap:Body', namespaces=NAMESPACES)

        # print(etree.tostring(self.header))
        # print(etree.tostring(self.body))
        print(self.header)
        print(self.body)

        # headers = self.tree.xpath(
        #     '//soap:Header', namespaces=NAMESPACES)
        # if len(headers) > 1:
        #     raise ICantEvenException('Unexpected quantity of SOAP Headers')
        # else:
        #     self.header = headers[0]


class SOAPHeader ():
    pass


class SOAPBody ():
    pass
